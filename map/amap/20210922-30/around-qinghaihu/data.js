const containerCenter = {
    zoom: 7,
    center: [101.778112, 36.617042]
};
const administrativeCenter = [{
    id: 1,
    title: "湟源县",
    position: [101.256464, 36.682427],
    awesomeIcon: "star",
    iconLabel: {style: {color: "salmon", fontSize: "14px"}},
    iconStyle: "black",
    infoBody: "",
    type: "行政区",
    visible: true
}, {
    id: 1,
    title: "西宁",
    position: [101.778112, 36.617042],
    awesomeIcon: "star",
    iconLabel: {style: {color: "salmon", fontSize: "14px"}},
    iconStyle: "black",
    infoBody: "",
    type: "行政区",
    visible: true
}];
const traffic = [{
    id: 4,
    title: "西宁曹家堡机场",
    position: [102.039941, 36.528964],
    awesomeIcon: "plane",
    iconLabel: {style: {color: "#333", fontSize: "14px"}},
    iconStyle: "orange",
    infoBody: "",
    type: "机场",
    visible: true
}];
const hotel = [];
const food = [];
const scenery = [{
    id: 1,
    title: "塔尔寺",
    position: [101.56773, 36.487506],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "日月山",
    position: [101.094385, 36.437716],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "二郎剑景区",
    position: [100.443029, 36.614709],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "黑马河",
    position: [99.763005, 36.71041],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "刚察县",
    position: [100.145833, 37.32547],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "金银滩原子城",
    position: [100.819998, 36.995502],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "沙岛湖",
    position: [100.517724, 36.888143],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "环湖东路南出口",
    position: [100.769618, 36.759105],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "倒淌河镇",
    position: [100.971643, 36.398166],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "金沙湾欢乐景区",
    position: [100.77464, 36.74382],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "甲乙寺",
    position: [100.710602, 36.51252],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "西海镇",
    position: [100.902973, 36.957868],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "原子城爆轰实验场",
    position: [100.819413, 36.996794],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "刚察沙柳河景区",
    position: [100.13348, 37.319401],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "鸟岛景区",
    position: [99.854771, 36.989309],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "仙女湾景区",
    position: [100.115466, 37.207274],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 1,
    title: "环湖西湖",
    position: [99.751566, 37.132326],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 2,
    title: "茶卡盐湖",
    position: [99.080168, 36.75387],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}];
const innerScenery = [];

const polylines = []


let data = [];
data.push(...administrativeCenter);
data.push(...traffic);
data.push(...hotel);
data.push(...food);
data.push(...scenery);
data.push(...innerScenery);

const containerCenter = {
    zoom: 7,
    center: [101.778112, 36.617042]
};
const administrativeCenter = [{
    id: 1,
    title: "湟源县",
    position: [101.256464, 36.682427],
    awesomeIcon: "star",
    iconLabel: {style: {color: "salmon", fontSize: "14px"}},
    iconStyle: "black",
    infoBody: "",
    type: "行政区",
    visible: true
}, {
    id: 1,
    title: "西宁",
    position: [101.778112, 36.617042],
    awesomeIcon: "star",
    iconLabel: {style: {color: "salmon", fontSize: "14px"}},
    iconStyle: "black",
    infoBody: "",
    type: "行政区",
    visible: true
}, {
    id: 1,
    title: "海晏县",
    position: [100.99443, 36.896467],
    awesomeIcon: "star",
    iconLabel: {style: {color: "salmon", fontSize: "14px"}},
    iconStyle: "black",
    infoBody: "",
    type: "行政区",
    visible: true
}, {
    id: 1,
    title: "祁连县",
    position: [100.253211, 38.177112],
    awesomeIcon: "star",
    iconLabel: {style: {color: "salmon", fontSize: "14px"}},
    iconStyle: "black",
    infoBody: "",
    type: "行政区",
    visible: true
}];
const traffic = [{
    id: 4,
    title: "西安咸阳国际机场",
    position: [108.764882, 34.438179],
    awesomeIcon: "plane",
    iconLabel: {style: {color: "#333", fontSize: "14px"}},
    iconStyle: "orange",
    infoBody: "",
    type: "机场",
    visible: true
}, {
    id: 4,
    title: "西宁曹家堡机场",
    position: [102.039941, 36.528964],
    awesomeIcon: "plane",
    iconLabel: {style: {color: "#333", fontSize: "14px"}},
    iconStyle: "orange",
    infoBody: "",
    type: "机场",
    visible: true
}];
const hotel = [];
const food = [{
    id: 74,
    title: "益鑫手抓(城中店)",
    position: [101.782481, 36.618532],
    awesomeIcon: "cutlery",
    iconLabel: {style: {color: "gray", fontSize: "14px"}},
    iconStyle: "lightpink",
    infoBody: "<div>手抓肉、手工酸奶、牛肉面</div><div style='text-align: right'>— — <a href='https://gs.ctrip.com/html5/you/travels/281/2518536.html'>携程某攻略</a></div>",
    type: "美食",
    visible: true
}];
const scenery = [{
    id: 1,
    title: "塔尔寺",
    position: [101.56773, 36.487506],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 2,
    title: "茶卡盐湖",
    position: [99.080168, 36.75387],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 3,
    title: "青海湖",
    position: [100.228654, 36.894097],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 3,
    title: "张掖丹霞国家地质公园",
    position: [100.088361, 38.956881],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 3,
    title: "莫高窟",
    position: [94.809374, 40.042511],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}, {
    id: 3,
    title: "祁连山草原",
    position: [100.943219, 37.963804],
    awesomeIcon: "photo",
    iconLabel: {style: {color: "lightgreen", fontSize: "14px"}},
    iconStyle: "green",
    infoBody: "",
    type: "景区",
    visible: true
}];
const innerScenery = [];

const polylines = []


let data = [];
data.push(...administrativeCenter);
data.push(...traffic);
data.push(...hotel);
data.push(...food);
data.push(...scenery);
data.push(...innerScenery);
